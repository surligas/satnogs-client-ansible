#!/bin/sh -e
#
# SatNOGS client setup configuration script
#
# Copyright (C) 2017-2019 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

VERSION_SATNOGS_CLIENT_ANSIBLE=$(cd "$HOME/.satnogs/ansible" && git show -s --format=%cd --date='format:%Y%m%d%H%M' || :)
VERSION_SATNOGS_CLIENT=$(/var/lib/satnogs/bin/pip show satnogs-client 2>/dev/null | awk '/^Version: / { print $2 }')
VERSION_GR_SATNOGS=$(dpkg-query --show -f='${Version}' gr-satnogs 2>/dev/null || :)

BACKTITLE="SatNOGS client configuration | Installed: satnogs-client-ansible-${VERSION_SATNOGS_CLIENT_ANSIBLE}${VERSION_SATNOGS_CLIENT:+, satnogs-client-$VERSION_SATNOGS_CLIENT}${VERSION_GR_SATNOGS:+, gr-satnogs-$VERSION_GR_SATNOGS}"
WIDTH="78"
YAMLFILE_PATH="${1:-/etc/ansible/host_vars/localhost}"
BOOTSTRAP_STAMP="$HOME/.satnogs/.bootstrapped"
INSTALL_STAMP="$HOME/.satnogs/.installed"
CONFIGURED_STAMP="$HOME/.satnogs/.configured"

MAIN_MENU="Basic:Basic configuration options:menu
Advanced:Advanced configuration options:menu
Show:Show configuration file:show
Upgrade:Upgrade system packages:upgrade
Update:Update configuration tool:update
Reset:Reset configuration:reset
Apply:Apply configuration:apply
About:Information about satnogs-setup:about"

BASIC_MENU="SATNOGS_API_TOKEN:Define API token:input
SATNOGS_RX_DEVICE:Define RX device:input
SATNOGS_STATION_ELEV:Define station elevation:input
SATNOGS_STATION_ID:Define station ID:input
SATNOGS_STATION_LAT:Define station latitude:input
SATNOGS_STATION_LON:Define station longitude:input
HAMLIB_UTILS_ROT_ENABLED:Enable Hamlib rotctld:yesno
HAMLIB_UTILS_ROT_OPTS:Define Hamlib rotctld options:input"

ADVANCED_MENU="$BASIC_MENU
SATNOGS_NETWORK_API_URL:Define network API URL:input
SATNOGS_PRE_OBSERVATION_SCRIPT:Define pre-observation script:input
SATNOGS_POST_OBSERVATION_SCRIPT:Define post-observation script:input
SATNOGS_APP_PATH:Define application data path:input
SATNOGS_OUTPUT_PATH:Define output data path:input
SATNOGS_COMPLETE_OUTPUT_PATH:Define completed data path:input
SATNOGS_INCOMPLETE_OUTPUT_PATH:Define incompleted data path:input
SATNOGS_REMOVE_RAW_FILES:Remove raw files:yesno
SATNOGS_VERIFY_SSL:Verify SatNOGS network SSL certificate:yesno
SATNOGS_SQLITE_URL:Define SQLite URI:input
SATNOGS_ROT_IP:Define Hamlib rotctld IP:input
SATNOGS_ROT_PORT:Define Hamlib rotctld port:input
SATNOGS_RIG_IP:Define Hamlib rigctld IP:input
SATNOGS_RIG_PORT:Define Hamlib rigctld port:input
SATNOGS_ROT_THRESHOLD:Define Hamlib rotctld command threshold:input
SATNOGS_DOPPLER_CORR_PER_SEC:Define rate of doppler correction (sec):input
SATNOGS_LO_OFFSET:Define local oscillator offset:input
SATNOGS_PPM_ERROR:Define PPM error (integer):input
SATNOGS_IF_GAIN:Define SatNOGS Radio IF Gain:input
SATNOGS_RF_GAIN:Define SatNOGS Radio RF Gain:input
SATNOGS_BB_GAIN:Define SatNOGS Radio BB Gain:input
SATNOGS_ANTENNA:Define SatNOGS Radio Antenna:input
SATNOGS_DEV_ARGS:Define SatNOGS Radio dev arguments:input
ENABLE_IQ_DUMP:Enable IQ dump:yesno
IQ_DUMP_FILENAME:Define IQ dump filename:input
DISABLE_DECODED_DATA:Disable decoded data:yesno
SATNOGS_RADIO_GR_SATNOGS_VERSION:Define gr-satnogs package version:input
APT_REPOSITORY:Define package repository:input
APT_KEY_URL:Define package repository key URL:input
APT_KEY_ID:Define package repository key ID:input
HAMLIB_UTILS_RIG_DISABLED:Disable Hamlib rigctld:yesno
HAMLIB_UTILS_RIG_OPTS:Define Hamlib rigctld options:input
SATNOGS_CLIENT_VERSION:Define SatNOGS client version:input
SATNOGS_CLIENT_URL:Define SatNOGS client Git URL:input
SATNOGS_SETUP_ANSIBLE_URL:Define Ansible Git URL:input
SATNOGS_SETUP_ANSIBLE_BRANCH:Define Ansible Git branch:input
SATNOGS_SETUP_RELEASE_UPGRADE_ENABLED:Enable release upgrades:yesno
SNMPD_ENABLED:Enable snmpd:yesno
SNMPD_AGENTADDRESS:Define snmpd agentAddress:input
SNMPD_ROCOMMUNITY:Define snmpd rocommunity:input
GPSD_ENABLED:Enable GPSd:yesno
SATNOGS_WATERFALL_AUTORANGE:Enable waterfall power range adjustment:yesno
SATNOGS_WATERFALL_MIN_VALUE:Define waterfall minimum power:input
SATNOGS_WATERFALL_MAX_VALUE:Define Waterfall maximum power:input"

to_lower() {
	tr '[:upper:]' '[:lower:]'
}

to_upper() {
	tr '[:lower:]' '[:upper:]'
}

get_tags_items_list() {
	menu="$1"

	echo "$menu" | awk 'BEGIN { FS=":" } { printf("\"%s\" \"%s\" ", $1, $2) }'
}

get_item() {
	menu="$1"
	tag="$2"

	get_menu "$1" | awk 'BEGIN { FS=":" } /'"$tag"'/ { print $2 }'
}

get_type() {
	menu="$1"
	tag="$2"

	get_menu "$1" | awk 'BEGIN { FS=":" } /'"$tag"'/ { print $3 }'
}

get_menu() {
	menu="$1"

	eval "echo \"\$$(echo "$menu" | to_upper)_MENU\""
}

get_variable() {
	file="$1"
	variable="$2"

	if [ -f "$file" ]; then
		awk 'BEGIN { FS="'"$variable"' *: *" } /^'"$variable"' *:/ { print $2 }' "$file"
	fi
}

set_variable() {
	file="$1"
	variable="$2"
	value="$3"

	if [ -f "$file" ]; then
		sed -i '/^'"$variable"' *:.*/ d' "$file"
	fi
	if [ -n "$value" ]; then
		echo "${variable}: ${value}" >> "$file"
		sort -o "$file" "$file"
	fi
}

menu() {
	title="$1"
	menu="$2"
	default="$3"

	set +e
	eval "whiptail \
		--clear \
		--backtitle \"$BACKTITLE\" \
		--title \"$title\" \
		--ok-button \"Select\" \
		--cancel-button \"Back\" \
		--default-item \"$default\" \
		--menu \"[UP], [DOWN] arrow keys to move\\n[ENTER] to select\" 0 0 0 \
		$(get_tags_items_list "$menu")"
	res=$?
	set -e
	if [ $res -eq 1 ] || [ $res -eq 255 ]; then
		echo "Back" 1>&2
	fi
}

input() {
	inputbox="$1"
	init="$2"

	set +e
	whiptail \
		--clear \
		--backtitle "$BACKTITLE" \
		--title "Parameter definition" \
		--ok-button "Ok" \
		--cancel-button "Cancel" \
		--inputbox "$inputbox" 0 "$WIDTH" -- "$init"
	res=$?
	set -e
	if [ $res -eq 1 ] || [ $res -eq 255 ]; then
		echo "Cancel" 1>&2
	fi
}

yesno() {
	yesno="$1"

	set +e
	whiptail \
		--clear \
		--backtitle "$BACKTITLE" \
		--title "Parameter definition" \
		--yes-button "Yes" \
		--no-button "No" \
		--yesno "$yesno" 0 0

	res=$?
	set -e
	if [ $res -eq 1 ] || [ $res -eq 255 ]; then
		echo "false" 1>&2
	else
		echo "true" 1>&2
	fi
}

exec 3>&1

tag="Main"
while true; do

	case $tag in
		Back)
			if [ "$menu" = "Main" ]; then
				if [ ! -f "$CONFIGURED_STAMP" ]; then
					set +e
					whiptail \
						--clear \
						--backtitle "$BACKTITLE" \
						--title "Exit without applying" \
						--yes-button "Yes" \
						--no-button "No" \
						--defaultno \
						--yesno "Are you sure you want to exit without applying configuration?" 0 0

					res=$?
					set -e
					if [ $res -ne 1 ] && [ $res -ne 255 ]; then
						exec 3>&-
						exit 0
					fi
				else
					exec 3>&-
					exit 0
				fi
			fi
			tag="Main"
			;;
		Show)
			if [ -f "$YAMLFILE_PATH" ]; then
				set +e
				whiptail \
					--clear \
					--backtitle "$BACKTITLE" \
					--title "SatNOGS client configuration" \
					--ok-button "Ok" \
					--scrolltext \
					--textbox "$YAMLFILE_PATH" 20 0
				set -e
			fi
			tag="Main"
			;;
		Basic|Advanced|Main)
			menu="$tag"
			tag=$(eval "menu \"$tag Menu\" \"\$$(echo "$tag" | to_upper)_MENU\" \"$item\" 2>&1 1>&3")
			item=""
			;;
		Update)
			rm -f "$BOOTSTRAP_STAMP" "$INSTALL_STAMP" "$CONFIGURED_STAMP"
			exec satnogs-setup
			;;
		Upgrade)
			exec 3>&-
			if satnogs-upgrade; then
				echo "Press enter to continue..."
				read -r _temp
				exec 3>&1
				set +e
				whiptail \
					--clear \
					--backtitle "$BACKTITLE" \
					--title "System reboot is required" \
					--yes-button "Yes" \
					--no-button "No" \
					--defaultno \
					--yesno "Do you want to reboot the system now?" 0 0

				res=$?
				set -e
				if [ $res -ne 1 ] && [ $res -ne 255 ]; then
					exec 3>&-
					sync
					reboot
				fi
			else
				echo "Press enter to continue..."
				read -r _temp
			fi
			exec satnogs-setup
			;;
		Reset)
			rm -f "$BOOTSTRAP_STAMP" "$INSTALL_STAMP" "$CONFIGURED_STAMP" "$YAMLFILE_PATH"
			exec satnogs-setup
			;;
		Apply)
			touch "$CONFIGURED_STAMP"
			exec 3>&-
			break
			;;
		About)
			set +e
			whiptail \
				--clear \
				--backtitle "$BACKTITLE" \
				--title "SatNOGS client configuration" \
				--ok-button "Ok" \
				--msgbox "satnogs-setup is a tool for configuring SatNOGS client system" 0 0
			set -e
			tag="Main"
			;;
		*)
			type=$(get_type "$menu" "$tag")
			item=$(get_item "$menu" "$tag")
			variable=$(echo "$tag" | to_lower)
			init=$(get_variable "$YAMLFILE_PATH" "$variable")
			input=$(eval "$type \"$item\" \"$init\" 2>&1 1>&3")
			if [ "$input" != "Cancel" ]; then
				set_variable "$YAMLFILE_PATH" "$variable" "$input"
				rm -f "$CONFIGURED_STAMP"
			fi
			item="$tag"
			tag="$menu"
			;;
	esac

done

exec 3>&-
